var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'iotropy'
    },
    port: 3000,
    db: 'postgres://postgres:unlock@localhost/iotropy-dev'
  },

  test: {
    root: rootPath,
    app: {
      name: 'iotropy'
    },
    port: 3000,
    db: 'postgres://postgres:unlock@localhost/iotropy-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'iotropy'
    },
    port: 3000,
    db: 'postgres://postgres:unlock@localhost/iotropy-prod'
  }
};

module.exports = config[env];
