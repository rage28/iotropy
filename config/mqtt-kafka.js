var WsStream = require('websocket-stream');
var WebSocketServer = require('ws').Server;
var mqCon = require('mqtt-connection');
var kafka = require('kafka-node');

module.exports = function (secureServer, secureSocketServer) {
  var wss = new WebSocketServer({
    server: secureServer
  });
  var sio = require('socket.io')(secureSocketServer);
  var KafkaProducer = kafka.Producer;
  var KafkaConsumer = kafka.Consumer;

  sio.on('connection', function (socket) {
    console.log('SocketIO Client Connected');

    var kafkaClient = new kafka.Client();
    var consumer = new KafkaConsumer(kafkaClient, [{
      topic: 'presence'
    }], {
      autoCommit: true
    });

    consumer.on('message', function (message) {
      console.log('Kafka Message Consumed: ' + JSON.stringify(message));
      socket.emit('device presence', message);
    });
  });

  wss.on('connection', function (ws) {
    console.log('WSS Connecting');
    var kafkaClient = new kafka.Client();
    var kafkaProducer = new KafkaProducer(kafkaClient);
    var mqClient = new mqCon(new WsStream(ws));

    ws.on('close', function close() {
      console.log('WSS disconnected');
      kafkaClient.close();
    });

    secureServer.on('mqtt-client', function (connection) {
      var client = connection;

      client.once('connect', function () {
        client.connack({
          returnCode: 0,
          sessionPresent: false
        });

        client.on('publish', function (packet) {
          console.log('Publishing Packet: ' + JSON.stringify(packet));

          var kafkaPayload = [{
            topic: packet.topic,
            messages: packet.payload.toString()
          }];

          kafkaProducer.send(kafkaPayload, function (err, data) {
            if (err) {
              console.log('Unable to publish to Kafka', err);
            } else {
              console.log('Published to Kafka: ' + JSON.stringify(data));
            }
          });
        });

        client.on('disconnect', function () {
          console.log('Disconnecting WSS Client');
          kafkaClient.close();
        });

        client.on('error', function (err) {
          console.log('Error in WSS Client', err);
          kafkaClient.close();
        });
      });
    });

    secureServer.emit('mqtt-client', mqClient);

    kafkaProducer.on('ready', function () {
      console.log('Kafka producer ready');

      kafkaProducer.on('error', function (err) {
        console.log('Something went wrong on Kafka: ' + err);
      });
    });
  });
};
