var express = require('express');
var read = require('fs').readFileSync;
var http = require('http');
var https = require('https');
var forceSSL = require('express-force-ssl');

var config = require('./config/config');
// var db = require('./app/models');
var credentials = {
  key: read('./docs/server_private_key.pem').toString(),
  cert: read('./docs/server_certificate.pem').toString()
};

var app = express();
var server = http.createServer(app);
var secureServer = https.createServer(credentials, app);
var secureSocketServer = https.createServer(credentials, app);

function redirectSecure(req, res, next) {
  if (req.secure) {
    next();
  } else {
    res.redirect('https://' + req.host + req.url);
  }
}

app.use(forceSSL);
app.use('*', redirectSecure);
require('./config/express')(app, config);
require('./config/mqtt-kafka.js')(secureServer, secureSocketServer);

//db.sequelize
//  .sync()
//  .then(function () {
//    app.listen(config.port);
//  }).catch(function (e) {
//    throw new Error(e);
//  });

server.listen(8080);
secureSocketServer.listen(8443);
secureServer.listen(8444);
