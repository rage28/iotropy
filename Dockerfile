# node image
FROM iojs:onbuild

# docker mantainer
MAINTAINER Raghavendra Bhuvan <rage28@gmail.com>

# add the files to load
ADD ./ .

# install all needed packages
RUN npm install --global gulp
RUN npm install --global bower
RUN bower install --allow-root
RUN npm install

# expose port
EXPOSE 8443

# execute app.js
ENTRYPOINT ["gulp"]
