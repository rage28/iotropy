var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var plumber = require('gulp-plumber');
var livereload = require('gulp-livereload');
var stylus = require('gulp-stylus');
var preen = require('preen');
var jpegtran = require('imagemin-jpegtran');
var pngquant = require('imagemin-pngquant');
var read = require('fs').readFileSync;

gulp.task('stylus', function () {
  gulp.src('./static_components/css/*.styl')
    .pipe(plumber())
    .pipe(stylus())
    .pipe(gulp.dest('./public/css'))
    .pipe(livereload());
});

gulp.task('watch', function () {
  gulp.watch('./static_components/css/*.styl', ['stylus']);
});

gulp.task('jimgopti', function () {
  return gulp.src('./static_components/img/*.jpg')
    .pipe(jpegtran({progressive: false})())
    .pipe(gulp.dest('./public/img/'));
});

gulp.task('pimgopti', function () {
  return gulp.src('./static_components/img/*.png')
    .pipe(pngquant({quality: '65-80', speed: 5})())
    .pipe(gulp.dest('./public/img/'));
});

gulp.task('preen', function (cb) {
  preen.preen({}, cb);
});

gulp.task('movejs', function () {
  return gulp.src('./static_components/js/*.js')
    .pipe(gulp.dest('./public/js'));
});

gulp.task('develop', function () {
  livereload.listen({
    key: read('./docs/server_private_key.pem'),
    cert: read('./docs/server_certificate.pem')
  });
  nodemon({
    script: 'app.js',
    ext: 'js coffee jade'
  }).on('restart', function () {
    setTimeout(function () {
      livereload.changed(__dirname);
    }, 500);
  });
});

gulp.task('default', [
  'preen',
  'jimgopti',
  'pimgopti',
  'stylus',
  'movejs',
  'develop',
  'watch'
]);
