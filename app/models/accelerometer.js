module.exports = function (sequelize, DataTypes) {

  var Accelerometer = sequelize.define('Accelerometer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    clientId: {
      type: DataTypes.STRING(72),
      primaryKey: true
    },
    xAxis: {
      type: DataTypes.DOUBLE(4),
      allowNull: false
    },
    yAxis: {
      type: DataTypes.DOUBLE(4),
      allowNull: false
    },
    zAxis: {
      type: DataTypes.DOUBLE(4),
      allowNull: false
    }
  }, {
    freezeTableNameName: true,
    tableName: 'Accelerometer'
  });

  return Accelerometer;
};
